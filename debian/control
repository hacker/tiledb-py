Source: tiledb-py
Section: python
Priority: optional
Uploaders: Adam Cecile <acecile@le-vert.net>, Dirk Eddelbuettel <edd@debian.org>, Nilesh Patra <nilesh@debian.org>
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 13), dh-python, python3-all-dev,
# From setup.py
 cmake,
 cython3 (>= 0.27~),
 python3-pybind11 (>= 2.6.2~),
 python3-numpy (>= 1.7~),
 python3-setuptools (>= 18.0~),
 python3-setuptools-scm (>= 1.5.4~),
 python3-wheel (>= 0.30~),
 libtiledb-dev (>= 2.7.0~),
 python3-pytest <!nocheck>,
# Check tiledb/tests folder
 python3-psutil <!nocheck>,
 python3-dask <!nocheck>, python3-distributed <!nocheck>,
 python3-pandas <!nocheck>,
 python3-hypothesis <!nocheck>,
# Packages below are not available in the archive yet
# python3-pyarrow <!nocheck>,
# python3-fastparquet <!nocheck>,
 python3-sphinx <!nodoc>, python3-sphinx-rtd-theme <!nodoc>,
# InterSphinx
 python3-doc <!nodoc>,
 libtiledb-doc <!nodoc>,
Standards-Version: 4.6.1
Homepage: https://tiledb.com/
Vcs-Git: https://salsa.debian.org/python-team/packages/tiledb-py.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/tiledb-py
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-tiledb
Architecture: any
Depends: ${shlibs:Depends}, ${python3:Depends}, ${misc:Depends}
Suggests: python3-tiledb-doc
Description: Python interface to the TileDB storage manager
 TileDB allows you to store and access very large multi-dimensional array
 data, the data currency of Data Science.
 .
 It is a powerful storage engine that introduces a novel format that can
 effectively store both dense and sparse array data with support for fast
 updates and reads.
 .
 This package installs the library for Python 3.

Package: python3-tiledb-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Python interface to the TileDB storage manager (documentation)
 TileDB allows you to store and access very large multi-dimensional array
 data, the data currency of Data Science.
 .
 It is a powerful storage engine that introduces a novel format that can
 effectively store both dense and sparse array data with support for fast
 updates and reads.
 .
 This is the Python interface documentation package.
